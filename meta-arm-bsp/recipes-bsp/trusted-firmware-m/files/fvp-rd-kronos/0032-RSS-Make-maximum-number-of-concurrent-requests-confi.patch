From f61396775dfaaea98f315fb067e98dad92ada499 Mon Sep 17 00:00:00 2001
From: Ziad Elhanafy <ziad.elhanafy@arm.com>
Date: Mon, 30 Oct 2023 12:56:34 +0000
Subject: [PATCH] RSS: Make maximum number of concurrent requests configurable

The maximum number of concurrent requests to RSS is hardcoded
to two. This value needs to be configurable as there are
four connections which may be established with
the RSS at the same time in Kronos:
1- Application Processor
2- Safety Island Cluster 0, 1 and 2.
Make this value configurable and set it to four in kronos so
none of the connections get a connection busy reply.

Upstream-Status: Pending
Signed-off-by: Ziad Elhanafy <ziad.elhanafy@arm.com>
---
 platform/ext/target/arm/rss/common/rss_comms/CMakeLists.txt | 2 +-
 platform/ext/target/arm/rss/kronos/config.cmake             | 1 +
 platform/ext/target/arm/rss/rdfremont/config.cmake          | 1 +
 platform/ext/target/arm/rss/tc/config.cmake                 | 2 ++
 4 files changed, 5 insertions(+), 1 deletion(-)

diff --git a/platform/ext/target/arm/rss/common/rss_comms/CMakeLists.txt b/platform/ext/target/arm/rss/common/rss_comms/CMakeLists.txt
index 622c98979..12a7f34a1 100644
--- a/platform/ext/target/arm/rss/common/rss_comms/CMakeLists.txt
+++ b/platform/ext/target/arm/rss/common/rss_comms/CMakeLists.txt
@@ -23,7 +23,7 @@ target_sources(platform_s
 
 target_compile_definitions(platform_s
     PRIVATE
-        RSS_COMMS_MAX_CONCURRENT_REQ=2
+        RSS_COMMS_MAX_CONCURRENT_REQ=${RSS_COMMS_MAX_CONCURRENT_REQ}
         RSS_COMMS_PROTOCOL_EMBED_ENABLED
         RSS_COMMS_PROTOCOL_POINTER_ACCESS_ENABLED
         $<$<BOOL:${CONFIG_TFM_HALT_ON_CORE_PANIC}>:CONFIG_TFM_HALT_ON_CORE_PANIC>
diff --git a/platform/ext/target/arm/rss/kronos/config.cmake b/platform/ext/target/arm/rss/kronos/config.cmake
index 35f392b2e..ba5380293 100644
--- a/platform/ext/target/arm/rss/kronos/config.cmake
+++ b/platform/ext/target/arm/rss/kronos/config.cmake
@@ -35,6 +35,7 @@ set(TFM_PARTITION_PROTECTED_STORAGE           ON   CACHE BOOL   "Enable Protecte
 set(TFM_PARTITION_INTERNAL_TRUSTED_STORAGE    ON   CACHE BOOL   "Enable Internal Trusted Storage partition")
 set(TFM_PARTITION_CRYPTO                      ON   CACHE BOOL   "Enable Crypto partition")
 set(TFM_MBEDCRYPTO_PLATFORM_EXTRA_CONFIG_PATH  ""  CACHE PATH   "Config to append to standard Mbed Crypto config, used by platforms to cnfigure feature support")
+set(RSS_COMMS_MAX_CONCURRENT_REQ               4   CACHE STRING "Maximum number of concurrent connection requests to RSS")
 
 # Once all options are set, set common options as fallback
 include(${CMAKE_CURRENT_LIST_DIR}/../common/config.cmake)
diff --git a/platform/ext/target/arm/rss/rdfremont/config.cmake b/platform/ext/target/arm/rss/rdfremont/config.cmake
index 445a5edf7..b99c08f6d 100644
--- a/platform/ext/target/arm/rss/rdfremont/config.cmake
+++ b/platform/ext/target/arm/rss/rdfremont/config.cmake
@@ -22,6 +22,7 @@ elseif(TFM_PLATFORM_VARIANT STREQUAL "2")
 else()
     set(RSS_LCP_COUNT                          16       CACHE STRING   "Number of LCPs to load")
 endif()
+set(RSS_COMMS_MAX_CONCURRENT_REQ               2        CACHE STRING   "Maximum number of concurrent connection requests to RSS")
 
 # Maximum number of MCUBoot images supported by TF-M NV counters and ROTPKs
 if(MCUBOOT_IMAGE_NUMBER GREATER 9)
diff --git a/platform/ext/target/arm/rss/tc/config.cmake b/platform/ext/target/arm/rss/tc/config.cmake
index f3e8b6ebf..e2e37013c 100644
--- a/platform/ext/target/arm/rss/tc/config.cmake
+++ b/platform/ext/target/arm/rss/tc/config.cmake
@@ -7,3 +7,5 @@
 
 # Once all options are set, set common options as fallback
 include(${CMAKE_CURRENT_LIST_DIR}/../common/config.cmake)
+
+set(RSS_COMMS_MAX_CONCURRENT_REQ   2   CACHE STRING "Maximum number of concurrent connection requests to RSS")
-- 
2.25.1

