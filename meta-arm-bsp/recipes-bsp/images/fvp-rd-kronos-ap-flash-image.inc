DEPENDS += "trusted-firmware-a fiptool-native"

inherit tfm_sign_image

TFA_BL2_BINARY = "bl2.bin"
TFA_BL2_IMAGE_LOAD_ADDRESS = "0x70095c00"
TFA_BL2_SIGN_BIN_SIZE = "0x80000"

TFA_FIP_BINARY = "fip.bin"
TFA_FIP_IMAGE_LOAD_ADDRESS = "0x70090000"
TFA_FIP_SIGN_BIN_SIZE = "0x200000"

do_sign_images() {
    # Sign TF-A BL2
    sign_host_image ${RECIPE_SYSROOT}/firmware/${TFA_BL2_BINARY} \
        ${TFA_BL2_IMAGE_LOAD_ADDRESS} ${TFA_BL2_SIGN_BIN_SIZE}

    # Update BL2 in the FIP image
    cp ${RECIPE_SYSROOT}/firmware/${TFA_FIP_BINARY} .
    fiptool update --tb-fw \
        ${TFM_IMAGE_SIGN_DEPLOY_DIR}/signed_${TFA_BL2_BINARY} \
        ${TFM_IMAGE_SIGN_DIR}/${TFA_FIP_BINARY}

    # Sign the FIP image
    sign_host_image ${TFM_IMAGE_SIGN_DIR}/${TFA_FIP_BINARY} \
        ${TFA_FIP_IMAGE_LOAD_ADDRESS} ${TFA_FIP_SIGN_BIN_SIZE}
}
