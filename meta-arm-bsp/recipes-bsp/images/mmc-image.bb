SUMMARY = "An image of MMC that can be used as removable storage"
DESCRIPTION = "A core-image based image recipe which can be used to generate \
               the image for MMC as a removable storage device."
LICENSE = "MIT"

inherit core-image deploy nopackages

do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_testimage[noexec] = "1"

IMAGE_CLASSES = ""
IMAGE_FEATURES = ""
PACKAGE_INSTALL = ""

WKS_FILE = "${MACHINE}-mmc-image.wks.in"
IMAGE_FSTYPES = "wic"
EXTRA_IMAGEDEPENDS ?= ""
