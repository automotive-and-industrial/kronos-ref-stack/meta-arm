UEFI_FIRMWARE_BINARY = "${PN}-${MACHINE}.${CAPSULE_IMGTYPE}"
UEFI_CAPSULE_CONFIG := "${THISDIR}/files/${MACHINE}-capsule-update-image.json"

# It is required to be configured in machine config
ARM_SYSTEMREADY_CAPSULE_UPDATE_IMAGES ?= ""

DEPENDS += "${ARM_SYSTEMREADY_CAPSULE_UPDATE_IMAGES}"

# Check if the required variables are set
python() {
    if not d.getVar("ARM_SYSTEMREADY_CAPSULE_UPDATE_IMAGES"):
        raise bb.parse.SkipRecipe("ARM_SYSTEMREADY_CAPSULE_UPDATE_IMAGES needs to be set")
}
