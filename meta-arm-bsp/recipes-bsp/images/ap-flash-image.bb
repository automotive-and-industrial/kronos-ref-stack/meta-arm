SUMMARY = "Platform AP (Application Processor) Flash Image"
DESCRIPTION = "This is the image which is the container of the FIP (Firmware \
               Image Package) image."
LICENSE = "MIT"

inherit core-image deploy nopackages

do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_testimage[noexec] = "1"

IMAGE_CLASSES = ""
IMAGE_FEATURES = ""
IMAGE_FSTYPES = "wic"
PACKAGE_INSTALL = ""

WKS_FILE = "${MACHINE}-ap-flash-image.wks"
EXTRA_IMAGEDEPENDS = ""
