IMAGE_BOOT_FILES:append = " \
    ${@bb.utils.contains('ARM_SYSTEMREADY_CAPSULE_UPDATE', '1', \
        'efi-capsule-update-image-${MACHINE}.wic.nopt.uefi.capsule;fw.cap', \
        '', d)} \
    "

do_image_wic[depends] += " \
    ${@bb.utils.contains('ARM_SYSTEMREADY_CAPSULE_UPDATE', '1',  \
        'efi-capsule-update-image:do_image_complete', '', d)} \
    "
