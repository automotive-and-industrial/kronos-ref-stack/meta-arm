# Include machine specific mmc-image configurations

MACHINE_MMC_IMAGE_REQUIRE ?= ""
MACHINE_MMC_IMAGE_REQUIRE:fvp-rd-kronos = "fvp-rd-kronos-mmc-image.inc"

require ${MACHINE_MMC_IMAGE_REQUIRE}
