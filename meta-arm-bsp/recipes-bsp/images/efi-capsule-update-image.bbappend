# Include machine specific efi-capsule-update-image configurations

MACHINE_EFI_CAPSULE_UPDATE_IMAGE_REQUIRE ?= ""
MACHINE_EFI_CAPSULE_UPDATE_IMAGE_REQUIRE:fvp-rd-kronos = "fvp-rd-kronos-efi-capsule-update-image.inc"

require ${MACHINE_EFI_CAPSULE_UPDATE_IMAGE_REQUIRE}
