# Include machine specific ap-flash-image configurations

MACHINE_AP_FLASH_IMAGE_REQUIRE ?= ""
MACHINE_AP_FLASH_IMAGE_REQUIRE:fvp-rd-kronos = "fvp-rd-kronos-ap-flash-image.inc"

require ${MACHINE_AP_FLASH_IMAGE_REQUIRE}
