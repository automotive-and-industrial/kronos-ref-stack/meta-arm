# Include machine specific RSS flash image configurations

MACHINE_RSS_FLASH_IMAGE_REQUIRE ?= ""
MACHINE_RSS_FLASH_IMAGE_REQUIRE:fvp-rd-kronos = "rss-flash-image-fvp-rd-kronos.inc"

require ${MACHINE_RSS_FLASH_IMAGE_REQUIRE}
