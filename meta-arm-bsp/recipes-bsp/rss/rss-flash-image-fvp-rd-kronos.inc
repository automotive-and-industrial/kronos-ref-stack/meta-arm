inherit tfm_sign_image

IMAGE_FSTYPES = "wic"

DEPENDS += "trusted-firmware-m \
            scp-firmware \
            ${SAFETY_ISLAND_C0_RECIPE} \
            ${SAFETY_ISLAND_C1_RECIPE} \
            ${SAFETY_ISLAND_C2_RECIPE} \
"

SCP_FIRMWARE_BINARY = "scp_ramfw.bin"
SCP_FIRMWARE_IMAGE_LOAD_ADDRESS = "0x70001c00"
SCP_FIRMWARE_SIGN_BIN_SIZE = "0x80000"
LCP_FIRMWARE_BINARY = "lcp_ramfw.bin"
LCP_FIRMWARE_IMAGE_LOAD_ADDRESS = "0x70083c00"
LCP_FIRMWARE_SIGN_BIN_SIZE = "0x10000"

SI_CL0_FIRMWARE_BINARY = "${SAFETY_ISLAND_C0_IMAGE}.bin"
SI_CL0_FIRMWARE_IMAGE_LOAD_ADDRESS = "0x70117c00"
SI_CL0_FIRMWARE_SIGN_BIN_SIZE = "0x200000"

SI_CL1_FIRMWARE_BINARY = "${SAFETY_ISLAND_C1_IMAGE}.bin"
SI_CL1_FIRMWARE_IMAGE_LOAD_ADDRESS = "0x70319c00"
SI_CL1_FIRMWARE_SIGN_BIN_SIZE = "0x400000"

SI_CL2_FIRMWARE_BINARY = "${SAFETY_ISLAND_C2_IMAGE}.bin"
SI_CL2_FIRMWARE_IMAGE_LOAD_ADDRESS = "0x7071bc00"
SI_CL2_FIRMWARE_SIGN_BIN_SIZE = "0x800000"

do_sign_images() {
    if [ "${SAFETY_ISLAND_C0_RECIPE}" = "baremetal-wfi" ]
    then
        cp ${RECIPE_SYSROOT}/firmware/baremetal-wfi.bin \
            ${RECIPE_SYSROOT}/firmware/${SI_CL0_FIRMWARE_BINARY}
        cp ${RECIPE_SYSROOT}/firmware/baremetal-wfi.bin \
            ${RECIPE_SYSROOT}/firmware/${SI_CL1_FIRMWARE_BINARY}
        cp ${RECIPE_SYSROOT}/firmware/baremetal-wfi.bin \
            ${RECIPE_SYSROOT}/firmware/${SI_CL2_FIRMWARE_BINARY}

    fi

    sign_host_image ${RECIPE_SYSROOT}/firmware/${SCP_FIRMWARE_BINARY} \
        ${SCP_FIRMWARE_IMAGE_LOAD_ADDRESS} ${SCP_FIRMWARE_SIGN_BIN_SIZE}
    sign_host_image ${RECIPE_SYSROOT}/firmware/${LCP_FIRMWARE_BINARY} \
        ${LCP_FIRMWARE_IMAGE_LOAD_ADDRESS} ${LCP_FIRMWARE_SIGN_BIN_SIZE}
    sign_host_image ${RECIPE_SYSROOT}/firmware/${SI_CL0_FIRMWARE_BINARY} \
        ${SI_CL0_FIRMWARE_IMAGE_LOAD_ADDRESS} ${SI_CL0_FIRMWARE_SIGN_BIN_SIZE}
    sign_host_image ${RECIPE_SYSROOT}/firmware/${SI_CL1_FIRMWARE_BINARY} \
        ${SI_CL1_FIRMWARE_IMAGE_LOAD_ADDRESS} ${SI_CL1_FIRMWARE_SIGN_BIN_SIZE}
    sign_host_image ${RECIPE_SYSROOT}/firmware/${SI_CL2_FIRMWARE_BINARY} \
        ${SI_CL2_FIRMWARE_IMAGE_LOAD_ADDRESS} ${SI_CL2_FIRMWARE_SIGN_BIN_SIZE}
}

create_empty_rss_nvm_image() {
    rm -f ${DEPLOY_DIR_IMAGE}/${RSS_NVM_IMAGE}
    touch ${DEPLOY_DIR_IMAGE}/${RSS_NVM_IMAGE}
}
do_image_wic[postfuncs] += "create_empty_rss_nvm_image"
