FILESEXTRAPATHS:prepend:corstone1000 := "${THISDIR}/corstone1000:"

COMPATIBLE_MACHINE:corstone1000 = "corstone1000"
SRC_URI:append:corstone1000  = " \
    file://0001-Add-stub-capsule-update-service-components.patch \
    file://0002-Fixes-in-AEAD-for-psa-arch-test-54-and-58.patch \
    file://0003-FMP-Support-in-Corstone1000.patch \
    file://0004-GetNextVariableName-Fix.patch     \
    file://0005-plat-corstone1000-add-compile-definitions-for-ECP_DP.patch \
    file://0006-plat-corstone1000-Use-the-stateless-platform-service.patch \
    file://0007-plat-corstone1000-Initialize-capsule-update-provider.patch \
    "


COMPATIBLE_MACHINE:n1sdp = "n1sdp"

FILESEXTRAPATHS:prepend:fvp-rd-kronos := "${THISDIR}/fvp-rd-kronos:"
COMPATIBLE_MACHINE:fvp-rd-kronos = "fvp-rd-kronos"
SRC_URI:append:fvp-rd-kronos = " \
    file://0001-Add-new-framework-for-kronos-platform.patch \
    file://0002-Add-mhu-v3-driver-for-se_proxy-SP.patch \
    file://0003-Add-new-rpc-caller-for-Protected-Storage-in-se_proxy.patch \
    file://0004-Increase-MHU-buffer-and-payload-size-for-rss_comms.patch \
    file://0005-Change-the-base-address-for-rss_comms-virtio.patch \
    file://0006-Add-new-rpc-caller-for-Crypto-Service-on-kronos-plat.patch \
    file://0007-Plat-fvp-rd-kronos-Change-default-smm-values.patch \
    file://0008-Modify-the-device-name-from-xx-virtio-to-xx-shmem.patch \
    file://0009-Add-stub-capsule-update-service-components.patch \
    file://0010-Fixes-in-AEAD-for-psa-arch-test-54-and-58.patch \
    file://0011-FMP-Support-in-Corstone1000.patch \
    file://0012-Plat-corstone1000-add-compile-definitions-for-ECP_DP.patch \
    file://0013-Plat-corstone1000-Use-the-stateless-platform-service.patch \
    file://0014-Plat-corstone1000-Initialize-capsule-update-provider.patch \
    file://0015-plat-Rename-symbol-names-to-common-ones.patch \
    file://0016-Plat-kronos-Enable-capsule-update-service.patch \
    file://0017-tools-b-test-Add-se-proxy-kronos-opteesp-to-b-test.patch \
    file://0018-Plat-kronos-Add-ns-interrupts-action-for-OP-TEE-3.22.patch \
    "
# The fvp-rd-kronos will reuse corstone1000 patches
FILESEXTRAPATHS:prepend:fvp-rd-kronos := "${THISDIR}/corstone1000:"
SRC_URI:append:fvp-rd-kronos = " \
    file://0004-GetNextVariableName-Fix.patch \
    "
EXTRA_OECMAKE:append:fvp-rd-kronos = " -DPLAT_MHU_VERSION=3"
