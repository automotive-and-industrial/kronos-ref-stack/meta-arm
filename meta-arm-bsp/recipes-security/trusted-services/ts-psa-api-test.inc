FILESEXTRAPATHS:prepend:corstone1000 := "${THISDIR}/corstone1000/psa-apitest:"
FILESEXTRAPATHS:prepend:fvp-rd-kronos := "${THISDIR}/corstone1000/psa-apitest:"
FILESEXTRAPATHS:prepend:fvp-rd-kronos := "${THISDIR}/fvp-rd-kronos/psa-apitest:"

include ts-arm-platforms.inc

SRC_URI:append:corstone1000  = " \
    file://0001-corstone1000-port-crypto-config.patch;patchdir=../psatest \
    file://0002-corstone1000-Disable-obsolete-algorithms.patch;patchdir=../psatest \
    file://0003-corstone1000-Disable-SHA512-384.patch;patchdir=../psatest \
    "

SRCREV_psatest:fvp-rd-kronos = "334e110528f975186e601fb35b6250e770635467"
PV:fvp-rd-kronos = "v23.06_API1.5_ADAC_EAC"

# The fvp-rd-kronos will reuse corstone1000 patches
SRC_URI:append:fvp-rd-kronos = " \
    file://0001-corstone1000-port-crypto-config.patch;patchdir=../psatest \
    file://0002-corstone1000-Disable-obsolete-algorithms.patch;patchdir=../psatest \
    file://0003-corstone1000-Disable-SHA512-384.patch;patchdir=../psatest \
    "

SRC_URI:append:fvp-rd-kronos = " \
    file://0001-kronos-Disable-failing-crypto-tests.patch;patchdir=../psatest \
    "
