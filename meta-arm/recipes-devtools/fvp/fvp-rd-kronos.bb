FVP_BUILD_NUMBER ?= "11.25.15"
PV = "${FVP_BUILD_NUMBER}"

require recipes-devtools/fvp/fvp-ecosystem.inc

MODEL = "RD-Kronos"
MODEL_CODE = "FVP_RD_Kronos"

FVP_OVERRIDE ?= ""
FVP_SERVER_URL ?= "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Automotive%20FVPs"
FVP_SERVER_USER ?= ""
FVP_SERVER_KEY ?= ""

SRC_URI = "${FVP_SERVER_URL}/${MODEL_CODE}_${PV_URL}_${FVP_ARCH}.tgz;user=${FVP_SERVER_USER};pswd=${FVP_SERVER_KEY};subdir=${BP};name=${BUILD_ARCH}"
SRC_URI[aarch64.sha256sum] = "4a3591ac89b66dde7be88b2726c05ebdd089f5781055cfbae18d278a0b15181a"
SRC_URI[x86_64.sha256sum] = "f17a5a6c3f89d2e71777e106ff7bf2fa46424353fbac37689bf3613bc5a7fa19"

LIC_FILES_CHKSUM = "file://license_terms/license_agreement.txt;md5=1a33828e132ba71861c11688dbb0bd16 \
                    file://license_terms/third_party_licenses/third_party_licenses.txt;md5=b9005e55057311e41efe02ccfea8ea72 \
                    file://license_terms/third_party_licenses/arm_license_management_utilities/third_party_licenses.txt;md5=c09526c02e631abb95ad61528892552d"

# Mark no COMPATIBLE_HOST for the target, so that FVP related recipes can't be
# run inside the FVP.
COMPATIBLE_HOST:class-target = "^$"

python() {
    if  d.getVar('FVP_OVERRIDE') == '1':
        d.setVarFlag('SRC_URI', 'aarch64.sha256sum', '')
        d.setVarFlag('SRC_URI', 'x86_64.sha256sum', '')
        d.setVar('BB_STRICT_CHECKSUM', 'ignore')
        d.setVar('LICENSE', 'CLOSED')
}
