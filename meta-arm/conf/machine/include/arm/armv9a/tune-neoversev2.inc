#
# Tune Settings for Neoverse-V2
#
DEFAULTTUNE ?= "neoversev2"

TUNEVALID[neoversev2] = "Enable Neoverse-V2 specific processor optimizations"
TUNE_CCARGS .= "${@bb.utils.contains('TUNE_FEATURES', 'neoversev2', ' -mcpu=neoverse-v2', '', d)}"

require conf/machine/include/arm/arch-armv9a.inc

# Little Endian base configs
AVAILTUNES                                         += "neoversev2 neoversev2-crypto"
ARMPKGARCH:tune-neoversev2                          = "neoversev2"
ARMPKGARCH:tune-neoversev2-crypto                   = "neoversev2-crypto"
TUNE_FEATURES:tune-neoversev2                       = "${TUNE_FEATURES:tune-armv9a} neoversev2"
TUNE_FEATURES:tune-neoversev2-crypto                = "${TUNE_FEATURES:tune-neoversev2} crypto"
PACKAGE_EXTRA_ARCHS:tune-neoversev2                 = "${PACKAGE_EXTRA_ARCHS:tune-armv9a} neoversev2"
PACKAGE_EXTRA_ARCHS:tune-neoversev2-crypto          = "${PACKAGE_EXTRA_ARCHS:tune-armv9a-crypto} neoversev2 neoversev2-crypto"
BASE_LIB:tune-neoversev2                            = "lib64"
BASE_LIB:tune-neoversev2-crypto                     = "lib64"
