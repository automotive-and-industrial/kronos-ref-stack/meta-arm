SUMMARY = "UEFI Capsule Image"
DESCRIPTION = "This is the image which is the container of the images \
               to be updated via UEFI UpdateCapsule runtime service."
LICENSE = "MIT"

inherit core-image deploy nopackages wic_nopt uefi_capsule

do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_testimage[noexec] = "1"

IMAGE_CLASSES = ""
IMAGE_FEATURES = ""
IMAGE_FSTYPES = "wic.nopt uefi_capsule"
PACKAGE_INSTALL = ""

WKS_FILE = "${MACHINE}-capsule.wks.in"
EXTRA_IMAGEDEPENDS = ""

CAPSULE_IMGTYPE = "wic.nopt"
