SUMMARY = "RSS Flash Image"
DESCRIPTION = "Image to load at boot in flash storage. It \
contains the images for TF-M BL2, TF-A BL1, SCP-firmware and other \
machine-specific images"

IMAGE_CLASSES = ""
IMAGE_FSTYPES = "wic wic.nopt"
IMAGE_FEATURES = ""
PACKAGE_INSTALL = ""

KERNELDEPMODDEPEND = ""
KERNEL_DEPLOY_DEPEND = ""

inherit core-image wic_nopt

WKS_FILE = "${MACHINE}-rss-flash-image.wks.in"
EXTRA_IMAGEDEPENDS = ""
