SUMMARY = "RSS ROM Image"
DESCRIPTION = "Image to load at boot in the RSS ROM. It contains BL1 from \
Trusted Firmware-M"

IMAGE_CLASSES = ""
IMAGE_FSTYPES = "wic wic.nopt"
IMAGE_FEATURES = ""
PACKAGE_INSTALL = ""

KERNELDEPMODDEPEND = ""
KERNEL_DEPLOY_DEPEND = ""

inherit core-image wic_nopt

WKS_FILES = "${MACHINE}-rss-rom-image.wks rss-rom-image.wks"
EXTRA_IMAGEDEPENDS = ""
WKS_FILE_DEPENDS += "trusted-firmware-m"
