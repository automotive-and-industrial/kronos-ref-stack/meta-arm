SUMMARY = "Baremetal WFI application"
DESCRIPTION = "A simple application which immediately enters a WFI loop"
LICENSE = "MIT"
LIC_FILES_CHKSUM ?= "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit meson deploy

SRC_URI = "file://src"
S = "${WORKDIR}/src"
FILES:${PN} = "/firmware"
SYSROOT_DIRS += "/firmware"

do_deploy() {
    cp ${D}/firmware/${PN}.bin ${DEPLOYDIR}/${PN}.bin
}
addtask deploy after do_install
