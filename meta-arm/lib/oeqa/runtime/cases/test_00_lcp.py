from oeqa.runtime.case import OERuntimeTestCase


class LcpTest(OERuntimeTestCase):
    console = 'lcp'

    def test_normal_boot(self):
        self.target.transition('on')
        self.target.expect(self.console,
                           r'\[FWK\] Module initialization complete!',
                           timeout=60)
        self.assertNotIn(b'[ERROR]', self.target.before(self.console))
