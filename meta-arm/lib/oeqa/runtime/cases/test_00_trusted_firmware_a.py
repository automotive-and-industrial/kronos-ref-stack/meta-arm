from oeqa.runtime.case import OERuntimeTestCase


class TrustedFirmwareTest(OERuntimeTestCase):
    console = 'tf-a'

    def test_normal_boot(self):
        self.target.transition('on')
        self.target.expect(self.console,
                           r'BL31: Preparing for EL3 exit to normal world',
                           timeout=30)
        self.assertNotIn(b'ERROR:', self.target.before(self.console))
